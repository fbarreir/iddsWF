#!/bin/bash

echo "###### debugging hostname #####"
hostname
pwd
ls
echo "###### debugging hostname done #####"

python $payload_dir/convert_input.py

echo "Start training"
horovodrun -np 2 --host-discovery-script $HD_DIR/discover_hosts.sh $payload_dir/train.py distributed
echo "Finish training"

echo "###### copying output.json #####"
cp $payload_dir/output.json $SHARED_DIR/payload_workdir/

echo "###### debugging output.json #####"
hostname
pwd
ls
cat output.json
echo "###### debugging output.json done #####"
