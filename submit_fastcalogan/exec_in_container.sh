FILE1=$1
echo FILE1 $FILE1
particle=`echo $FILE1 | cut -d '_' -f 1`
eta_min=`echo $FILE1 | cut -d '_' -f 2`
eta_max=`echo $FILE1 | cut -d '_' -f 3`
#curl -sSL https://cernbox.cern.ch/index.php/s/B5W7g9rHCBeAhK7/download | tar -xzvf -;

#python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/dataset_test/ -ip $particle -emin $eta_min -emax $eta_max

echo "ls"
ls
echo "---------==========----------"
echo "ls user"
ls user.zhangr*
echo "---------==========----------"
echo "ls *"
ls *
echo "---------==========----------"
echo "cat input_ds"
cat input_ds.json
echo "---------==========----------"
echo "cat input"
cat input.json
echo "---------==========----------"
echo "untar input"
tar -xzf pid*.tar
echo "---------==========----------"
echo "ls"
ls
echo "---------==========----------"
rm -f pid*.tar
echo "ls *"
ls *
echo "---------==========----------"

echo python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i dataset -ip $particle -emin $eta_min -emax $eta_max
python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i ./ -ip $particle -emin $eta_min -emax $eta_max
echo "Finish training"
