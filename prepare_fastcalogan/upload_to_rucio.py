#!/usr/bin/env python
# Rui Zhang 2.2021
# rui.zhang@cern.ch

from __future__ import print_function
import sys
import os
import tarfile
import glob
import subprocess

def execute(cmd):
    """
    Executes a command in a subprocess. Returns a tuple
    of (exitcode, out, err), where out is the string output
    from stdout and err is the string output from stderr when
    executing the command.

    :param cmd: Command string to execute
    """

    process = subprocess.Popen(cmd,
                               shell=True,
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    result = process.communicate()
    (out, err) = result
    exitcode = process.returncode

    return exitcode, out.decode(), err.decode()

def compress(output_file="archive.tar.gz", items=[]):
    """compress dirs.

    KWArgs
    ------
    output_file : str, default ="archive.tar.gz"
    items : list
        list of dirs/items relative to root dir

    """
    print('Compress to tarball: {0}'.format(output_file))
    with tarfile.open(output_file, "w:gz") as tar:
        for i, item in enumerate(items):
            tar.add(item, arcname=os.path.basename(item))
            print('File: {0}/{1}, {2}'.format(i, len(items), item))

def tar_name(file_name, version):
    tar_full_name = file_name.split('/')[-1].split('.')[0].split('_')
    print(tar_full_name)
    tar_full_name.pop(-1)
    tar_full_name.pop(1)
    tar_full_name = '_'.join(tar_full_name)
    tar_full_name = '/tmp/zhangr/' + tar_full_name + '.' + version + '.tar'
    return tar_full_name

def register_dataset(dataset_name):
    # 2. Register a dataset for it
    cmd = 'rucio add-dataset ' + dataset_name
    print(cmd)
    try:
        exitcode, out, err = execute(cmd)
        print(out, err)
    except:
        pass

def upload_file(dataset_name, tar_full_name, command_only = True):
    # 1. upload csv file individually to rucio:
    tar_file_name = tar_full_name.split('/')[-1]
    cmd = 'rucio upload --rse BNL-OSG2_SCRATCHDISK {0} --name {1}'.format(tar_full_name, tar_file_name)
    print(cmd)
    if not command_only:
        exitcode, out, err = execute(cmd)
        print(out, err)

    # 3. Attach files to the dataset name (note that the file name has user.${RUCIO_ACCOUNT})
    cmd = 'rucio attach ' + dataset_name + ' user.{0}:'.format(os.environ['RUCIO_ACCOUNT']) + tar_file_name
    print(cmd)
    if not command_only:
        exitcode, out, err = execute(cmd)
        print(out, err)

def run(pid, eta, version, clean=True):
    files = glob.glob('/afs/cern.ch/user/z/zhangr/wis/test/csvFiles/pid{0}_*_eta_{1}_{2}_voxalisation.csv'.format(pid, eta[0], eta[1]))
    print(files[0])
    
    tar_full_name = tar_name(files[0], version)
    if clean and os.path.exists(tar_full_name):
        os.remove(tar_full_name)
    
    compress(tar_full_name, files)
    
    upload_file(dataset_name, tar_full_name, False)
    
    if clean and os.path.exists(tar_full_name):
        os.remove(tar_full_name)

def run_all():
    for pid in pids:
        for eta in eta_ranges:
            run(pid, eta, version)
    # 4. Attach user.zhangr:binning.xml
    cmd = 'rucio attach ' + dataset_name + ' user.{0}:'.format(os.environ['RUCIO_ACCOUNT']) + 'binning.xml'
    print(cmd)
    exitcode, out, err = execute(cmd)
    print(out, err)

def run_one(tar_name):
    # pid22_eta_0_5.v01.tar
    base = tar_name.split('.')[0].split('_')
    eta = (base[2], base[3])
    pid = base[0][3:]
    run(pid, eta, version, clean=False)

version = 'v02'

dataset_name = 'user.{0}:user.{0}.fastcalogan.{1}.dataset'.format(os.environ['RUCIO_ACCOUNT'], version)
register_dataset(dataset_name)


pids = ['11', '22', '211']
eta_ranges = [
        (0, 5),
        (20, 25),
        (80, 85),
        (100, 105),
        (200, 205),
        (220, 225),
        (300, 305),
        (400, 405),
    ]

run_all()
#run_one('pid22_eta_0_5.v02.tar')

print('rucio list-files '+dataset_name)
