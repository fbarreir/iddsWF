#phpo --loadJson config_dev.json --site  ANALY_CERN-PTEST --outDS user.$USER.horovod-cpu.`uuidgen`  --nParallelEvaluation 1 --nPointsPerIteration 1 --maxPoints 3 --maxEvaluationJobs 1 --searchSpaceFile search_space.json

phpo --loadJson config_gpu.json --site  ANALY_BNL_GPU_ARC --architecture nvidia-gpu --outDS user.$USER.horovod-1gpuBNL.`uuidgen`  --nParallelEvaluation 1 --nPointsPerIteration 1 --maxPoints 2 --maxEvaluationJobs 2 --searchSpaceFile search_space.json
phpo --loadJson config_gpu.json --site  ANALY_MANC_GPU_TEST --architecture nvidia-gpu --outDS user.$USER.horovod-1gpuMAN.`uuidgen`  --nParallelEvaluation 1 --nPointsPerIteration 1 --maxPoints 2 --maxEvaluationJobs 2 --searchSpaceFile search_space.json
#phpo --loadJson config_gpu.json --site  ANALY_SLAC_GPU --architecture nvidia-gpu --outDS user.$USER.horovod-1gpuSLC.`uuidgen`  --nParallelEvaluation 1 --nPointsPerIteration 1 --maxPoints 2 --maxEvaluationJobs 2 --searchSpaceFile search_space.json
