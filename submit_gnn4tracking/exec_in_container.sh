#!/bin/bash

current_dir=$(pwd)

# These variables are used in the payload
export payload_dir=/ATLASMLHPO/payload/DLGNN4Tracking/
export TRKXOUTPUTDIR=${current_dir}"/output"
export TRKXINPUTDIR=${current_dir}"/train_10evts"

# curl -sSL https://cernbox.cern.ch/index.php/s/PKXAwYTfTM5gb6V/download | tar -xzvf -;
tar xvfz submit_horovod_dataset10evts.tar

# Convert idds point to payload format
python /ATLASMLHPO/payload/DLGNN4Tracking/convert_input.py

# Run training
python /ATLASMLHPO/payload/DLGNN4Tracking/train.py

# Cleanup (may not needed)
rm -fr /__pycache__/
