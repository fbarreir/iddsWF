pwd
pip install IPython
echo "ls -l"
ls -l
echo 
echo "ls -l v04.trt_sharded_weighted_1M5K.tar.gz"
ls -lh `realpath v04.trt_sharded_weighted_1M5K.tar.gz`
echo "untar"
tar xvfz v04.trt_sharded_weighted_1M5K.tar.gz
echo "untar done"
echo "ls -l scratch"
ls -l /scratch
echo "/scratch/condor_pool/condor/"
ls -l /scratch/condor_pool/condor/

ls -l
pwd

echo "ls -l /"
ls -l /
echo "ls -l /ATLASMLHPO"
ls -l /ATLASMLHPO
#python /ATLASMLHPO/payload/RNN_TRT/TestRun_iDDS.py --config_file /ATLASMLHPO/payload/RNN_TRT/config_mlflow.json --hp_input input.json
python /ATLASMLHPO/payload/RNN_TRT/TestRun_iDDS.py --config_file /ATLASMLHPO/payload/RNN_TRT/config_local.json --hp_input input.json
tar cvfz metrics.tgz mlruns/*
echo "Finish training"
