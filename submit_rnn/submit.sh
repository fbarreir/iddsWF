#phpo --loadJson config_dev.json --site ANALY_MWT2_GPU,ANALY_MANC_GPU_TEST,ANALY_BNL_GPU_ARC --outDS user.$USER.`uuidgen`  --nParallelEvaluation 2 --nPointsPerIteration 2 --maxPoints 4 --searchSpaceFile search_space.json
#phpo --loadJson config_dev.json --outDS user.$USER.`uuidgen`  --nParallelEvaluation 10 --nPointsPerIteration 10 --maxPoints 10 --searchSpaceFile search_space.json
phpo --loadJson config_dev.json --site  ANALY_CERN-PTEST --outDS user.$USER.rnn.`uuidgen`  --nParallelEvaluation 1 --nPointsPerIteration 1 --maxPoints 10 --searchSpaceFile search_space.json
#phpo --loadJson config_dev.json --site ANALY_MANC_GPU_TEST --architecture nvidia-gpu --outDS user.$USER.rnn.`uuidgen`  --nParallelEvaluation 2 --maxPoints 4 --searchSpaceFile search_space.json
#phpo --loadJson config_dev.json --site  ANALY_BNL_GPU_ARC --outDS user.$USER.rnn.`uuidgen`  --nParallelEvaluation 1 --nPointsPerIteration 2 --maxPoints 3 --searchSpaceFile search_space.json
