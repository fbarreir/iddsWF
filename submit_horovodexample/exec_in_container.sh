#!/bin/bash

echo "###### debugging hostname #####"
hostname
pwd
ls
echo "###### debugging hostname done #####"

echo "Start training"
horovodrun -np 2 --host-discovery-script $HD_DIR/discover_hosts.sh python /ATLASMLHPO/payload/HorovodSimpleExamples/mnist/train_mnist_horovod.py --fraction 0.01 --epochs 3 --log-dir $SHARED_DIR/payload_workdir/distributed
echo "Finish training"

# cp $payload_dir/output.json $SHARED_DIR/payload_workdir/

echo "###### debugging output.json #####"
hostname
pwd
ls
cat output.json
echo "###### debugging output.json done #####"

